百度音乐下载助手
------
>致力于 **免费、自由、开源** 软件的开发共享 ---- **有一份田** 作品
****
## 1.百度音乐下载助手-----简介
**百度音乐下载助手**  是一个结构简单,功能强大的百度音乐助手,他能帮助你突破 **百度音乐会员**   限制,自由下载 **高品质** 音乐。
****
>####效果如下:
>####音乐浏览页面的效果:
>![music helper][1]
>
>****
>
>####音乐下载页面的效果:
>![music helper][2]
>****
>##2. 关于我们
>* 为了 **"自由，开源，共享"** 的精神
>* 本代码可自由传播分享,但 **转播请注明出处**
>* 官网：http://www.duoluohua.com/download/
>* Git: http://git.oschina.net/youyifentian/
>* 邮箱：youyifentian@gmail.com

  [1]: http://duoluohua.com/myapp/chrome/baidumusic/images/options_1.png
  [2]: http://duoluohua.com/myapp/chrome/baidumusic/images/options_2.png

